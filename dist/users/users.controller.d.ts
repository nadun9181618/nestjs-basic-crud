import { CreateUser } from './dtos/create-user.dto';
import { UsersService } from './users.service';
import { UpdateUser } from './dtos/update-user.dto';
export declare class UsersController {
    private readonly userService;
    constructor(userService: UsersService);
    create(createUser: CreateUser): Promise<import("./user.entity").User>;
    findOne(id: string): Promise<import("./user.entity").User>;
    update(id: string, updateUser: UpdateUser): Promise<{
        email: string;
        name: string;
        id: number;
    } & import("./user.entity").User>;
    remove(id: string): Promise<import("./user.entity").User>;
}
