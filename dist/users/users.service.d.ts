import { CreateUser } from './dtos/create-user.dto';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { UpdateUser } from './dtos/update-user.dto';
export declare class UsersService {
    private readonly userRepo;
    constructor(userRepo: Repository<User>);
    create(createUser: CreateUser): Promise<User>;
    findOne(id: number): Promise<User>;
    update(id: number, updateUser: UpdateUser): Promise<{
        email: string;
        name: string;
        id: number;
    } & User>;
    remove(id: number): Promise<User>;
}
